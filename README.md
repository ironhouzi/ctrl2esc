# ctrl2esc

## Why?!

Some people use nice keyboards with a properly situated left ctrl, but still want dual role ctrl/escape. E.g. [IBM Model F](https://goo.gl/images/HxbwJe) or [KBParadise V60](https://imgur.com/gallery/WdX6qy0)
Even though the changes are quite small compared to the original plugin, I though it could be of use to others who don't know C programming, or don't want to spend the time.

## Dependencies

- [Interception Tools][interception-tools]

## Building

```sh
$ git clone git@gitlab.com:ironhouzi/ctrl2esc.git
$ cd ctrl2esc
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Installation

```sh
$ sudo make install
```

## Execution

`ctrl2esc` is an [_Interception Tools_][interception-tools] plugin. A suggested
`udevmon` job configuration is:

```yaml
- JOB: "intercept -g $DEVNODE | ctrl2esc | uinput -d $DEVNODE"
  DEVICE:
    EVENTS:
      EV_KEY: [KEY_LEFTCTRL]

```

For more information about the [_Interception Tools_][interception-tools], check
the project's website.

## License

<a href="https://gitlab.com/interception/linux/plugins/caps2esc/blob/master/LICENSE.md">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/License_icon-mit-2.svg/120px-License_icon-mit-2.svg.png" alt="MIT">
</a>

Copyright © 2017 Francisco Lopes da Silva

[interception-tools]: https://gitlab.com/interception/linux/tools
