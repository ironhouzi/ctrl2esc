#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// clang-format off
const struct input_event
esc_up          = {.type = EV_KEY, .code = KEY_ESC,      .value = 0},
ctrl_up         = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 0},
esc_down        = {.type = EV_KEY, .code = KEY_ESC,      .value = 1},
ctrl_down       = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 1},
syn             = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1) {
        exit(EXIT_FAILURE);
    }
}

int main(void) {
    int ctrl_down_state = 0, esc_deadline = 0;
    struct input_event input;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN) {
            continue;
        }

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        if (ctrl_down_state) {
            if (equal(&input, &ctrl_up)) {
                ctrl_down_state = 0;

                if (esc_deadline) {
                    esc_deadline = 0;
                    write_event(&ctrl_up);
                    continue;
                }

                write_event(&esc_down);
                write_event(&syn);
                usleep(20000);
                write_event(&esc_up);
                continue;
            }

            if (!esc_deadline && input.value) {
                esc_deadline = 1;
                write_event(&ctrl_down);
                write_event(&syn);
                usleep(20000);
            }
        } else if (equal(&input, &ctrl_down)) {
            ctrl_down_state = 1;
            continue;
        }

        write_event(&input);
    }
}
